const AbstractService = require('./abstract-service');
const User = require('../model').User;
const serializer = require('./serialization-service');

class UserService extends AbstractService {

  static getUser(id) {
    return User.findById(id)
      .then(user => serializer.serializeUser(user));
  }

}

module.exports = UserService;