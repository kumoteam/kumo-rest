class SerializationService {

  static serializeUser(user) {
    const userObj = user.get({plain: true});
    return {
      username: userObj.username,
      email: userObj.email,
      createdAt: userObj.createdAt,
      updatedAt: userObj.updatedAt
    };
  }
}

module.exports = SerializationService;