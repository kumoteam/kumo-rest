const UserService = require('./user-service');
const AuthService = require('./auth-service');

module.exports.UserService = UserService;
module.exports.AuthService = AuthService;

module.exports.Service = {
  AuthService,
  UserService
};