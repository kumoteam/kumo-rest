const AbstractService = require('./abstract-service');
const User = require('../model').User;
const CredentialsError = require('../util/credentials-error');
const sha256 = require('sha256');
const hmacsha256 = require('crypto-js/hmac-sha256');
const btoa = require('btoa');
const atob = require('atob');

const secret = 'Catwoman_123';

function credentailsAreValid(username, email) {
  const emailMatch = email.match(/^\w+@\w+\.\w+$/g);
  return username && username.length > 2 && email && emailMatch;
}

function encryptPassword(password) {
  return sha256(password);
}

function createToken(username) {
  const header = { alg: 'HS256', typ: 'JWT' };

  const payload = { username };

  const encHeader = btoa(JSON.stringify(header));
  const encPayload = btoa(JSON.stringify(payload));

  const unsignedToken = encHeader + '.' + encPayload;
  const signature = hmacsha256(unsignedToken, secret);
  const encSignature = btoa(JSON.stringify(signature));

  return `${encHeader}.${encPayload}.${encSignature}`;
}

class AuthService extends AbstractService {

  static register(username, email, password) {
    return User.findOne({where: {id: username}})
      .then(user => {
        if (user)
          throw new CredentialsError(`User ${username} already exists`);
        if (!credentailsAreValid(username, email))
          throw new CredentialsError('User credentials are invalid!');

        return User.build({
          username: username,
          email: email,
          password: encryptPassword(password)
        }).save();
      });
  }

  static login(username, password) {
    return User.findOne({ where: { username } })
      .then(user => {
        if (!user)
          throw new CredentialsError(`User ${username} does not exists`);
        if (user.password !== encryptPassword(password))
          throw new CredentialsError(`Wrong password for ${username}`);

        const token = createToken(username, user.role);
        return token;
      });
  }

  static tokenValid(token) {
    if (!token)
      return false;
    const payload = AuthService.getPayload(token);

    const username = payload.username;

    return createToken(username) === token;
  }

  static getPayload(token) {
    return JSON.parse(atob(token.split('.')[1]));
  }

}

module.exports = AuthService;