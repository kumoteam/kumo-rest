const express = require('express');
const app = express();

require('./config')(app);

const port = +process.argv[2];

if (!port || isNaN(port))
  throw new Error();

require('./model');
require('./router')(app);

app.listen(port, () => {
  console.log(`Deployed on ${port}`);
});