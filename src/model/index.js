const Sequelize = require('sequelize');
const dbconfig = require('../config/dbconfig');

const sequelize = new Sequelize(dbconfig);

const User = require('./user')(sequelize);

sequelize.sync();

module.exports.sequelize = sequelize;
module.exports.User = User;
