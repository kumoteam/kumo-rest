const AuthService = require('../service').AuthService;
const router = require('express').Router();
const CredentialsError = require('../util/credentials-error');

router.post('/login', (req, res) => {
  AuthService.login(req.body.username, req.body.password)
    .then(token => res.status(200).json({ token }).end())
    .catch(e => {
      if (e instanceof CredentialsError)
        res.status(401).json({ error: e.message }).end();
      else
        throw e;
    }).catch(e => {
      res.status(500).json(e.message).end();
    });
});

router.post('/register', (req, res) => {
  AuthService.register(
    req.body.username,
    req.body.email,
    req.body.password
  ).then(user => {
    const result = {
      username: user.dataValues.username,
      email: user.dataValues.email
    };
    res.status(200).json(result).end();
  }).catch(e => {
    if (e instanceof CredentialsError)
      res.status(401).json({error: e.message}).end();
    else
      throw e;
  })
    .catch(e => res.status(500).json({ error: e.message }).end());
});

module.exports = router;