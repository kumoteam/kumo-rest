const authRouter = require('./auth-router');
const userRouter = require('./user-router');

const baseUrl = '/api';
const userUrl = `${baseUrl}/user`;

module.exports = (app) => {
  app.use(authRouter);
  app.use(userUrl, userRouter);
};

module.exports.baseUrl = baseUrl;
