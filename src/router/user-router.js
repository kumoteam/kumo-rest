const UserService = require('../service').UserService;
const router = require('express').Router();
const handleError = require('../util/handle-error');

router.get('/:id', (req, res) => {
  UserService.getUser(req.params.id)
    .then(user => res.status(200).json(user).end())
    .catch(handleError(res));
});

module.exports = router;