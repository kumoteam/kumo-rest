const bodyParser = require('body-parser');
const cors = require('./cors');
const logger = require('./logger');
// const securityService = require('./security');

module.exports = (app) => {
  app.use(cors);
  app.use(bodyParser.json());
  app.use(logger);
  // securityService.addSecurity(app);
};
